package net.pl3x.bukkit.time.time;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Time {
    private final long ticks;
    private final String standardTime;
    private final String militaryTime;

    public Time(long ticks) {
        this.ticks = ticks;

        // clamp ticks 0 - 24000
        if (ticks > 24000) {
            ticks = ticks % 24000;
        }
        if (ticks < 0) {
            ticks = 24000 + (ticks % 24000);
        }

        // get military time
        DecimalFormat df = new DecimalFormat("00");
        df.setRoundingMode(RoundingMode.DOWN);
        float hours = ticks / 1000 + 6;
        float minutes = (ticks % 1000) * 60 / 1000;
        while (hours >= 24) {
            hours -= 24;
        }
        this.militaryTime = df.format(hours) + ":" + df.format(minutes);

        // convert to standard time
        String meridian = hours >= 12 ? "PM" : "AM";
        if (hours > 12) {
            hours -= 12;
        }
        if (df.format(hours).equals("00")) {
            hours = 12;
        }
        this.standardTime = df.format(hours) + ":" + df.format(minutes) + " " + meridian;
    }

    public long getTicks() {
        return ticks;
    }

    public String getStandardTime() {
        return standardTime;
    }

    public String getMilitaryTime() {
        return militaryTime;
    }

    @Override
    public String toString() {
        return "Time[ticks:" + ticks +
                ", standard:" + standardTime +
                ", military:" + militaryTime + "]";
    }
}
