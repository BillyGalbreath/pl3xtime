package net.pl3x.bukkit.time.time;

import net.pl3x.bukkit.time.configuration.Config;
import org.bukkit.World;

import java.util.HashSet;
import java.util.Set;

public class TimeUtil {
    private static final Set<World> worldLocked = new HashSet<>();

    public static boolean isWorldLocked(World world) {
        return worldLocked.contains(world);
    }

    public static void lockWorld(World world) {
        worldLocked.add(world);
    }

    public static void unlockWorld(World world) {
        worldLocked.remove(world);
    }

    public static int getDay(World world) {
        return (int) Math.floor(world.getFullTime() / 24000);
    }

    public static Time translateTime(String string) {
        string = string.toLowerCase();
        try {
            return new Time(Long.parseLong(string));
        } catch (Exception e) {
            boolean add12 = false;
            if (string.endsWith("am")) {
                string = string.substring(0, string.length() - 2);
            }
            if (string.endsWith("a")) {
                string = string.substring(0, string.length() - 1);
            }
            if (string.endsWith("pm")) {
                add12 = true;
                string = string.substring(0, string.length() - 2);
            }
            if (string.endsWith("p")) {
                add12 = true;
                string = string.substring(0, string.length() - 1);
            }
            try {
                long ticks;
                if (string.contains(":")) {
                    String[] split = string.split(":");
                    int hr = Integer.parseInt(split[0].trim());
                    int mn = Integer.parseInt(split[1].trim());
                    if (hr < 0 || hr > 24 || mn < 0 || mn > 59) {
                        return null;
                    }
                    ticks = ((hr + (add12 ? 6 : -6)) * 1000) + (long) Math.ceil((mn / 60D) * 1000D);
                } else {
                    int hr = Integer.parseInt(string.trim());
                    if (hr < 0 || hr > 24) {
                        return null;
                    }
                    ticks = ((hr + (add12 ? 6 : -6)) * 1000);
                }
                return new Time(ticks);
            } catch (Exception e2) {
                return Config.PRESET_TIMES.get(string);
            }
        }
    }
}
