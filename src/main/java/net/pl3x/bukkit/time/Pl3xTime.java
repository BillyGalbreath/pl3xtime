package net.pl3x.bukkit.time;

import net.pl3x.bukkit.time.command.CmdDay;
import net.pl3x.bukkit.time.command.CmdPl3xTime;
import net.pl3x.bukkit.time.command.CmdSetDay;
import net.pl3x.bukkit.time.command.CmdSetTime;
import net.pl3x.bukkit.time.command.CmdTime;
import net.pl3x.bukkit.time.configuration.Config;
import net.pl3x.bukkit.time.configuration.Lang;
import net.pl3x.bukkit.time.hook.DiscordSRVHook;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xTime extends JavaPlugin {
    private DiscordSRVHook discordSRVHook;
    private final Logger logger;

    public Pl3xTime() {
        logger = new Logger(this);
    }

    public Logger getLog() {
        return logger;
    }

    @Override
    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        if (getServer().getPluginManager().isPluginEnabled("DiscordSRV")) {
            discordSRVHook = new DiscordSRVHook();
        }

        getCommand("day").setExecutor(new CmdDay());
        getCommand("time").setExecutor(new CmdTime());
        getCommand("setday").setExecutor(new CmdSetDay(this));
        getCommand("settime").setExecutor(new CmdSetTime(this));
        getCommand("pl3xtime").setExecutor(new CmdPl3xTime(this));

        getLog().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        getLog().info(getName() + " disabled.");
    }

    public DiscordSRVHook getDiscordSRVHook() {
        return discordSRVHook;
    }
}
