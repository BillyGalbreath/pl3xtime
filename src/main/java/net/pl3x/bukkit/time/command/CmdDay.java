package net.pl3x.bukkit.time.command;

import net.pl3x.bukkit.time.configuration.Lang;
import net.pl3x.bukkit.time.time.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class CmdDay implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Bukkit.getWorlds().stream()
                    .filter(world -> world.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    .map(World::getName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.day")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        World world;
        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                Lang.send(sender, Lang.PLAYER_COMMAND);
                return true;
            }
            world = ((Player) sender).getWorld();
        } else {
            world = Bukkit.getWorld(args[0]);
            if (world == null) {
                Lang.send(sender, Lang.WORLD_NOT_FOUND);
                return true;
            }
            if (!sender.hasPermission("command.day.*") &&
                    !sender.hasPermission("command.day." + world.getName())) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
        }

        Lang.send(sender, Lang.DAY_IS
                .replace("{world}", world.getName())
                .replace("{total-ticks}", Long.toString(world.getFullTime()))
                .replace("{day}", Integer.toString(TimeUtil.getDay(world))));
        return true;
    }
}
