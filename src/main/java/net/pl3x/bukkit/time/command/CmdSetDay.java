package net.pl3x.bukkit.time.command;

import net.pl3x.bukkit.time.Pl3xTime;
import net.pl3x.bukkit.time.configuration.Lang;
import net.pl3x.bukkit.time.time.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class CmdSetDay implements TabExecutor {
    private final Pl3xTime plugin;

    public CmdSetDay(Pl3xTime plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Bukkit.getWorlds().stream()
                    .filter(world -> world.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    .map(World::getName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.setday")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length == 0) {
            Lang.send(sender, Lang.MUST_SPECIFY_DAY);
            return false;
        }

        World world;
        String dayString;
        if (args.length > 1) {
            world = Bukkit.getWorld(args[0]);
            if (world == null) {
                Lang.send(sender, Lang.WORLD_NOT_FOUND);
                return true;
            }
            if (!sender.hasPermission("command.setday.*") &&
                    !sender.hasPermission("command.setday." + world.getName())) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
            dayString = args[1];
        } else {
            if (!(sender instanceof Player)) {
                Lang.send(sender, Lang.PLAYER_COMMAND);
                return true;
            }
            world = ((Player) sender).getWorld();
            dayString = args[0];
        }

        if (TimeUtil.isWorldLocked(world)) {
            Lang.send(sender, Lang.WORLD_TIME_CHANGE_IN_PROGRESS);
            return true;
        }

        int dayNum;
        try {
            dayNum = Integer.valueOf(dayString);
        } catch (NumberFormatException e) {
            Lang.send(sender, Lang.INVALID_DAY_SPECIFIED);
            return false;
        }

        long totalTicks = 24000 * dayNum + world.getTime();
        world.setFullTime(totalTicks);

        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', Lang.BROADCAST_DAY_SET
                .replace("{world}", world.getName())
                .replace("{total-ticks}", Long.toString(totalTicks))
                .replace("{day}", Integer.toString(dayNum))));

        if (plugin.getDiscordSRVHook() != null) {
            plugin.getDiscordSRVHook().sendToDiscord(Lang.BROADCAST_DISCORD_DAY_SET
                    .replace("{world}", world.getName())
                    .replace("{total-ticks}", Long.toString(totalTicks))
                    .replace("{day}", Integer.toString(dayNum)));
        }

        plugin.getLog().debug("Day for " + world.getName() + " set to " + dayNum);
        return true;
    }
}
