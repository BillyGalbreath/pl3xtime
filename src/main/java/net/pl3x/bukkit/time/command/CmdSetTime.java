package net.pl3x.bukkit.time.command;

import net.pl3x.bukkit.time.Pl3xTime;
import net.pl3x.bukkit.time.configuration.Lang;
import net.pl3x.bukkit.time.task.SmoothTime;
import net.pl3x.bukkit.time.time.Time;
import net.pl3x.bukkit.time.time.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class CmdSetTime implements TabExecutor {
    private final Pl3xTime plugin;

    public CmdSetTime(Pl3xTime plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Bukkit.getWorlds().stream()
                    .filter(world -> world.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    .map(World::getName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.settime")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length == 0) {
            Lang.send(sender, Lang.MUST_SPECIFY_TIME);
            return false;
        }

        World world;
        String timeString;
        if (args.length > 1) {
            world = Bukkit.getWorld(args[0]);
            if (world == null) {
                Lang.send(sender, Lang.WORLD_NOT_FOUND);
                return true;
            }
            if (!sender.hasPermission("command.settime.*") &&
                    !sender.hasPermission("command.settime." + world.getName())) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
            timeString = args[1];
        } else {
            if (!(sender instanceof Player)) {
                Lang.send(sender, Lang.PLAYER_COMMAND);
                return true;
            }
            world = ((Player) sender).getWorld();
            timeString = args[0];
        }

        Time time = TimeUtil.translateTime(timeString);
        if (time == null) {
            Lang.send(sender, Lang.INVALID_TIME_SPECIFIED);
            return false;
        }

        if (TimeUtil.isWorldLocked(world)) {
            Lang.send(sender, Lang.WORLD_TIME_CHANGE_IN_PROGRESS);
            return true;
        }

        TimeUtil.lockWorld(world);

        new SmoothTime(plugin, time, world)
                .runTaskAsynchronously(plugin);

        Lang.send(sender, Lang.CHANGING_TIME_TO
                .replace("{world}", "" + world.getName())
                .replace("{time}", "" + time.getStandardTime())
                .replace("{standard-time}", "" + time.getStandardTime())
                .replace("{military-time}", "" + time.getMilitaryTime())
                .replace("{ticks}", Long.toString(time.getTicks()))
                .replace("{day}", Integer.toString(TimeUtil.getDay(world))));

        plugin.getLog().debug("Time for " + world.getName() + " set to " + time);
        return true;
    }
}
