package net.pl3x.bukkit.time.task;

import net.pl3x.bukkit.time.Pl3xTime;
import net.pl3x.bukkit.time.configuration.Config;
import net.pl3x.bukkit.time.configuration.Lang;
import net.pl3x.bukkit.time.time.Time;
import net.pl3x.bukkit.time.time.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

public class SmoothTime extends BukkitRunnable {
    private final Pl3xTime plugin;
    private final Time time;
    private final World world;

    public SmoothTime(Pl3xTime plugin, Time time, World world) {
        this.plugin = plugin;
        this.time = time;
        this.world = world;
    }

    @Override
    public void run() {
        int speed = Config.SMOOTH_TIME_SPEED;
        int failsafeCounter = 0;

        // speed up time until we reach desired time
        for (long i = world.getTime() + 1; i != time.getTicks(); i++) {
            if (failsafeCounter > 24010) {
                break; // just in case we have a runaway infinite loop.
            }

            // roll over time to 0 of next day
            if (i == 24001) {
                i = 0;
                if (time.getTicks() == 0) {
                    break; // we desired the next day, so stop
                }
            }

            world.setTime(i);

            // slow down the task so its effects are visualized time speed up
            // slower speed pauses more often here (default pause 1ms every 5 cycles)
            if (i % speed == 0) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException ignore) {
                }
            }

            failsafeCounter++;
        }

        // sync back to main thread and set the official time
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            world.setTime(time.getTicks());
            TimeUtil.unlockWorld(world);

            Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', Lang.BROADCAST_TIME_SET
                    .replace("{world}", "" + world.getName())
                    .replace("{time}", "" + time.getStandardTime())
                    .replace("{standard-time}", "" + time.getStandardTime())
                    .replace("{military-time}", "" + time.getMilitaryTime())
                    .replace("{ticks}", Long.toString(time.getTicks()))
                    .replace("{day}", Integer.toString((int) Math.floor(world.getFullTime() / 24000)))));

            if (plugin.getDiscordSRVHook() != null) {
                plugin.getDiscordSRVHook().sendToDiscord(Lang.BROADCAST_DISCORD_TIME_SET
                        .replace("{world}", "" + world.getName())
                        .replace("{time}", "" + time.getStandardTime())
                        .replace("{standard-time}", "" + time.getStandardTime())
                        .replace("{military-time}", "" + time.getMilitaryTime())
                        .replace("{ticks}", Long.toString(time.getTicks()))
                        .replace("{day}", Integer.toString((int) Math.floor(world.getFullTime() / 24000))));
            }

        }, 1);
    }
}
