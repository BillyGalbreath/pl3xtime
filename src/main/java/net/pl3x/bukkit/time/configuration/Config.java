package net.pl3x.bukkit.time.configuration;

import net.pl3x.bukkit.time.time.Time;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class Config {
    public static boolean COLOR_LOGS;
    public static boolean DEBUG_MODE;
    public static String LANGUAGE_FILE;
    public static int SMOOTH_TIME_SPEED;
    public final static Map<String, Time> PRESET_TIMES = new HashMap<>();

    private Config() {
    }

    public static void reload(JavaPlugin plugin) {
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        SMOOTH_TIME_SPEED = config.getInt("smooth-time-speed", 5);

        PRESET_TIMES.clear();
        ConfigurationSection section = config.getConfigurationSection("preset-times");
        for (String key : section.getKeys(false)) {
            PRESET_TIMES.put(key.toLowerCase(), new Time(section.getInt(key)));
        }
    }
}
