package net.pl3x.bukkit.time.configuration;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION;
    public static String PLAYER_COMMAND;

    public static String WORLD_NOT_FOUND;
    public static String MUST_SPECIFY_TIME;
    public static String MUST_SPECIFY_DAY;
    public static String INVALID_DAY_SPECIFIED;
    public static String INVALID_TIME_SPECIFIED;
    public static String WORLD_TIME_CHANGE_IN_PROGRESS;

    public static String DAY_IS;
    public static String BROADCAST_DAY_SET;
    public static String BROADCAST_DISCORD_DAY_SET;
    public static String TIME_IS;
    public static String CHANGING_TIME_TO;
    public static String BROADCAST_TIME_SET;
    public static String BROADCAST_DISCORD_TIME_SET;

    public static String VERSION;
    public static String RELOAD;

    private Lang() {
    }

    public static void reload(JavaPlugin plugin) {
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        plugin.saveResource(Config.LANGUAGE_FILE, false);
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for that command");
        PLAYER_COMMAND = config.getString("player-command", "&4Player only command");

        WORLD_NOT_FOUND = config.getString("world-not-found", "&4World not found!");
        MUST_SPECIFY_TIME = config.getString("must-specify-time", "&4You must specify the time to set!");
        MUST_SPECIFY_DAY = config.getString("must-specify-day", "&4You must specify the day to set!");
        INVALID_DAY_SPECIFIED = config.getString("invalid-day-specified", "&4Unrecognized day number!");
        INVALID_TIME_SPECIFIED = config.getString("invalid-time-specified", "&4Unrecognized time format!");
        WORLD_TIME_CHANGE_IN_PROGRESS = config.getString("world-time-change-in-progress", "&4World time change is already in progress!");

        DAY_IS = config.getString("day-is", "&d{world} is on day {day} ({total-ticks} ticks)");
        BROADCAST_DAY_SET = config.getString("broadcast-day-set", "&dDay for {world} has been set to {day} ({total-ticks} ticks)");
        BROADCAST_DISCORD_DAY_SET = config.getString("broadcast-discord-day-set", ":watch: **Day for {world} has been set to {day}**");
        TIME_IS = config.getString("time-is", "&dThe time for {world} is {military-time} ({standard-time} [{ticks} ticks])");
        CHANGING_TIME_TO = config.getString("changing-time-to", "&dSetting time in {world} to {military-time} ({standard-time} [{ticks} ticks])");
        BROADCAST_TIME_SET = config.getString("broadcast-time-set", "&dTime for {world} has been set to {military-time} ({standard-time} [{ticks} ticks])");
        BROADCAST_DISCORD_TIME_SET = config.getString("broadcast-discord-time-set", ":watch: **Time for {world} has been set to {military-time} ({standard-time})**");

        VERSION = config.getString("version", "&d{plugin} v{version}");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
